#include <../functions_pack.h>
#include "tst_test_functions.h"


Test_functions::Test_functions()
{

}

bool compare_Complex_number(Complex_number first_number, Complex_number second_number)
{
    if (first_number.real_part == second_number.real_part && first_number.imaginary_part == second_number.imaginary_part)
    {
        return true;
    }
    else {
         return false;
    }

}

bool compare_Complex_number_for_another_form(Complex_number_another_form first_number, Complex_number_another_form second_number)
{
    if (first_number.module == second_number.module && first_number.argument == second_number.argument)
    {
        return true;
    }
    else {
         return false;
    }

}

bool compare_number_for_radical(QVector<Complex_number_another_form> vector_1, QVector<Complex_number_another_form> vector_2)
{
    if (vector_1.size() != vector_2.size())
    {
        return false;
    }
    for (int i = 0; i < vector_1.size(); i++)
    {
        if (vector_1[i].module != vector_2[i].module || vector_1[i].argument != vector_2[i].argument)
        {
            return false;
        }
    }
    return true;
}


void Test_functions::test_case1()           //тесты для функции суммы двух комплексных чисел
{
    Complex_number first_number;
    first_number.real_part = 2;
    first_number.imaginary_part = 3;

    Complex_number second_number;
    second_number.real_part = 4;
    second_number.imaginary_part = 1;

    Complex_number result_number;
    result_number.real_part = 6;        //first_number.real_part + second_number.real_part
    result_number.imaginary_part = 4;       //first_number.imaginary_part + second_number.imaginary_part

    QCOMPARE(true, compare_Complex_number(sum_number(first_number, second_number), result_number));

    result_number.real_part = 7;
    result_number.imaginary_part = 10;
    QCOMPARE(false, compare_Complex_number(sum_number(first_number, second_number), result_number));
}

void Test_functions::test_case2()   //тесты для проверки функции разности двух комплексных чисел
{
    Complex_number first_number;
    first_number.real_part = 2;
    first_number.imaginary_part = 3;

    Complex_number second_number;
    second_number.real_part = 4;
    second_number.imaginary_part = 1;

    Complex_number result_number;
    result_number.real_part = -2;        //first_number.real_part - second_number.real_part
    result_number.imaginary_part = 2;       //first_number.imaginary_part - second_number.imaginary_part

    QCOMPARE(true, compare_Complex_number(difference_number(first_number, second_number), result_number));
    QCOMPARE(false, compare_Complex_number(difference_number(second_number, first_number), result_number));

    result_number.real_part = 2;
    result_number.imaginary_part = -2;
    QCOMPARE(true, compare_Complex_number(difference_number(second_number, first_number), result_number));

}

void Test_functions::test_case3()    //тесты для проверки функции проверки равенства двух чисел
{
    Complex_number first_number;
    first_number.real_part = 6;
    first_number.imaginary_part = 3;

    Complex_number second_number;
    second_number.real_part = 6;
    second_number.imaginary_part = 3;

    QCOMPARE(true, equality_test(first_number, second_number));

    second_number.real_part = 4;
    second_number.imaginary_part = 3;
    QCOMPARE(false, equality_test(first_number, second_number));
}

void Test_functions::test_case4()      // тесты для проверки функции возведения в степень
{
    Complex_number_another_form test_number;
    test_number.module = 1;
    test_number.argument = atan(1);

    QCOMPARE(false, compare_Complex_number_for_another_form( test_number, exponentiation_number(test_number, 3)));

    //возьмём степень 3
    Complex_number_another_form result_number;
    result_number.module = 1;
    result_number.argument = 3 * atan(1);

    QCOMPARE(true, compare_Complex_number_for_another_form( result_number, exponentiation_number(test_number, 3)));

}

void Test_functions::test_case5()       //тесты для проверки функции умножения двух комплекных чисел
{
    Complex_number first_number;
    first_number.real_part = 3;
    first_number.imaginary_part = -2;

    Complex_number second_number;
    second_number.real_part = 5;
    second_number.imaginary_part = 3;

    Complex_number result_number;
    result_number.real_part = 21;
    result_number.imaginary_part = -1;

    QCOMPARE(true, compare_Complex_number( result_number, product_number(first_number, second_number)));

    QCOMPARE(false, compare_Complex_number( result_number, product_number(first_number, first_number)));
}

void Test_functions::test_case6()           //тесты для проверки функции вычисления корня из комплексного числа
{
    Complex_number_another_form number;
    number.argument = 0;
    number.module = 1;

    int degree = 3;

    QVector<Complex_number_another_form> result;
    result.resize(degree);
    result[0].module = 1;
    result[0].argument = 0;
    result[1].module = 1;
    result[1].argument = 4 * atan(1);
    result[2].module = 1;
    result[2].argument = 8 * atan(1);

    QCOMPARE(true, compare_number_for_radical( result, radical(number, degree)));

    QCOMPARE(false, compare_number_for_radical( result, radical(number, degree + 1)));
}

void Test_functions::test_case7()
{
    Complex_number_another_form first_number;
    first_number.module = 2;
    first_number.argument = 1;

    Complex_number_another_form second_number;
    second_number.module = 3;
    second_number.argument = 0;

    Complex_number_another_form result;
    result.module = 6;
    result.argument = 1;

    QCOMPARE(true, compare_Complex_number_for_another_form( result, pow_for_another_form(first_number, second_number)));

    QCOMPARE(false, compare_Complex_number_for_another_form( result, pow_for_another_form(first_number, first_number)));
}

void Test_functions::test_case8()
{
    Complex_number_another_form first_number;
    first_number.module = 6;
    first_number.argument = 1;

    Complex_number_another_form second_number;
    second_number.module = 3;
    second_number.argument = 1;

    Complex_number_another_form result;
    result.module = 2;
    result.argument = 0;

    QCOMPARE(true, compare_Complex_number_for_another_form( result, division_for_another_form(first_number, second_number)));

    QCOMPARE(false, compare_Complex_number_for_another_form( result, division_for_another_form(first_number, first_number)));
}
