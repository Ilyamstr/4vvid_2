#include "functions_pack.h"
#include <math.h>

/**
 \@brief Функция для нахождения суммы двух комплексных чисел.
 * Используется при сложении двух комплексных чисел в алгебраической форме.
 * @param first_number - первое число;
 * @param second_number - второе число;
 * @return result_sum - результат сложения чисел.
 */
Complex_number sum_number(Complex_number first_number, Complex_number second_number)
{
    Complex_number result_sum;
    result_sum.real_part = first_number.real_part + second_number.real_part;
    result_sum.imaginary_part = first_number.imaginary_part + second_number.imaginary_part;

    return  result_sum;
}

/**
 \@brief Функция для нахождения разности двух комплексных чисел.
 * Используется при вычитании двух комплексных чисел в алгебраической форме.
 * @param first_number - первое число;
 * @param second_number - второе число;
 * @return result_difference - результат вычитания чисел.
 */
Complex_number difference_number(Complex_number first_number, Complex_number second_number)
{
    Complex_number result_difference;
    result_difference.real_part = first_number.real_part - second_number.real_part;
    result_difference.imaginary_part = first_number.imaginary_part - second_number.imaginary_part;

    return result_difference;
}

/**
 \@brief Функция для проверки на равенство двух комплексных чисел.
 * Используется при сложении двух комплексных чисел в алгебраической форме.
 * @param first_number - первое число;
 * @param second_number - второе число;
 *
 * Для того, чтобы два комплексных числа были равны, необходимо равенство их действительных и мнимых частей соответственно.
 */
bool equality_test(Complex_number first_number, Complex_number second_number)
{
    if (first_number.real_part == second_number.real_part && first_number.imaginary_part == second_number.imaginary_part)
    {
        return true;
    }
    else {
         return false;
    }
}

/**
 \@brief Функция для возведения в степень комплексного числа.
 * Для данной функции необходима тригонометрическая или показательная форма комплексного числа.
 * @param number - данное число;
 * @param degree - показатель степени;
 * @return result_number - результат возведения комплексного числа в степень.
 */
Complex_number_another_form exponentiation_number(Complex_number_another_form number, double degree)
{
    Complex_number_another_form result_number;
    result_number.module = pow(number.module, degree);
    result_number.argument = number.argument * degree;

    return result_number;
}

/**
 \@brief Функция для нахождения произведения двух комплексных чисел в алгебраической форме.
 * Используется при умножении двух комплексных чисел в алгебраической форме.
 * @param first_number - первое число;
 * @param second_number - второе число;
 * @return result_number - результат умножения чисел.
 */
Complex_number product_number(Complex_number first_number, Complex_number second_number)
{
    Complex_number result_number;
    result_number.real_part = (first_number.real_part * second_number.real_part) - (first_number.imaginary_part *
                                                                                    second_number.imaginary_part);
    result_number.imaginary_part = (first_number.real_part * second_number.imaginary_part) + (second_number.real_part *
                                                                                              first_number.imaginary_part);

    return result_number;
}

/**
 \@brief Функция для возведения в степень комплексного числа.
 * Для данной функции необходима тригонометрическая или показательная форма комплексного числа.
 * @param number - данное число;
 * @param degree - показатель корня;
 * @return temp - результат извлечения корня из комплексного числа.
 *
 * Возвращаемое значение является массивом, согласно правилу извлечения корня из комплексного числа.
 */
QVector<Complex_number_another_form> radical(Complex_number_another_form number, double degree)
{
    QVector<Complex_number_another_form> temp;
    temp.resize(degree);
    double radical = 1 / degree;
    for (int k = 0; k < degree; k++)
    {
        temp[k].module = pow(number.module, radical);
        temp[k].argument = (number.argument + 8 * atan(1) * k) / 2;
    }
    return temp;
}

/**
 \@brief Функция для перевода комплексного числа из тригонометрической или показательной в алгебраическую форму.
 * Для данной функции необходима тригонометрическая или показательная форма комплексного числа.
 * Данная функция необходима из-за наличия функций, с которыми алгебраической форме числа не справиться.
 * @param module - модуль комплексного числа в тригонометрической или показательной форме;
 * @param argument -аргумент комплексного числа в тригонометрической или показательной форме;
 * @param real_part - действительная часть комплексного числа в алгебраической форме;
 * @param imaginary_part - мнимая часть комплексного числа в алгебраической форме;
 * @return функция ничего не возвращает. Она сразу изменяет необходимые параметры.
 */
void transform_number_from_another_form(double module, double argument, double& real_part, double& imaginary_part)
{
    while (argument >= 2)
    {
        argument = argument - 2;
    }
    argument = argument * 4 * atan(1);
    real_part = module * cos(argument);
    imaginary_part = module * sin(argument);

    if (argument == 4 * atan(1) || argument == 0)
    {
        imaginary_part = 0;
    }
    if (argument == 2 * atan(1) || argument == 6 * atan(1))
    {
        real_part = 0;
    }
}


/**
 \@brief Функция для перевода комплексного числа в тригонометрическую или показательную форму из алгебраической.
 * Для данной функции необходима алгебраическая форма комплексного числа.
 * Данная функция необходима из-за наличия функций, с которыми удобнее работать в алгебраической форме числа.
 * @param module - модуль комплексного числа в тригонометрической или показательной форме;
 * @param argument -аргумент комплексного числа в тригонометрической или показательной форме;
 * @param real_part - действительная часть комплексного числа в алгебраической форме;
 * @param imaginary_part - мнимая часть комплексного числа в алгебраической форме;
 * @return функция ничего не возвращает. Она сразу изменяет необходимые параметры.
 */
void transform_number(double& module, double& argument, double real_part, double imaginary_part)
{
    module = sqrt(pow(real_part, 2) + pow(imaginary_part, 2));
    argument = atan(imaginary_part / real_part) / (4 * atan(1));

    if (real_part < 0 && imaginary_part < 0)
    {
        argument++;
    }
    else if (real_part < 0)
    {
        if (argument < 0)
        {
            argument = argument * (-1);
        }
        argument = 1 - argument;
    }
    else if (imaginary_part < 0)
    {
        argument = argument + 2;
    }
}

/**
 \@brief Функция для нахождения частного двух комплексных чисел в тригонометрической или показательной форме.
 * Используется при делении двух комплексных чисел в тригонометрической или показательной форме.
 * @param first_number - первое число;
 * @param second_number - второе число;
 * @return result - результат деления чисел.
 */
Complex_number_another_form division_for_another_form(Complex_number_another_form first_number,Complex_number_another_form second_number)
{
    Complex_number_another_form result;
    result.module = first_number.module / second_number.module;
    result.argument = first_number.argument - second_number.argument;

    return result;
}

/**
 \@brief Функция для нахождения произведения двух комплексных чисел в тригонометрической или показательной форме.
 * Используется при умножении двух комплексных чисел в тригонометрической или показательной форме.
 * @param first_number - первое число;
 * @param second_number - второе число;
 * @return result - результат умножения чисел.
 */
Complex_number_another_form pow_for_another_form(Complex_number_another_form first_number,Complex_number_another_form second_number)
{
    Complex_number_another_form result;
    result.module = first_number.module * second_number.module;
    result.argument = first_number.argument + second_number.argument;

    return result;
}
